import Foundation

class Mi8{
    private var camera = String()
    private var processor = String()

    func addCamera(camera: String) {
        self.camera = camera
    }

    func addProcessor(processor: String) {
        self.processor = processor
    }

    func getData() -> String{
        return "Camara:" + self.camera + "\nProcesador:"  + self.processor
    }
}

class Mi9{ 
    private var camera = String()
    private var processor = String()
    
    func addCamera(camera: String) {
        self.camera = camera
    }

    func addProcessor(processor: String) {
        self.processor = camera
    }
}

protocol BuildCellphone{
    func addCamera()
    func addProcessor()
}

class BuildMi8:BuildCellphone{

    private var product = Mi8()

    func reset(){
        product = Mi8()
    }

    func addCamera(){
        product.addCamera(camera:"12MP")
    }

    func addProcessor(){
        product.addProcessor(processor:"2.4Hz")
    }

    func getProduct() -> Mi8{
        var cellphone = self.product
        self.reset()
        return cellphone
    } 
}


class Cliente{
    static func createMi8(){
        let builder = BuildMi8()
        builder.addCamera()
        builder.addProcessor()
        var a = builder.getProduct()
        print(a.getData())
    }
}

Cliente.createMi8()