
import Foundation

public class Shape {
    
    let graphicsApi: GraphicsAPI
    
    init(_ graphicsApi: GraphicsAPI) {
        self.graphicsApi = graphicsApi
    }
    
    func draw() -> Void {
        
    }
}

public class Circle : Shape {
    var x: Int = 0
    var y: Int = 0
    var radius: Int = 0
    
    convenience init(_ x: Int, _ y: Int, _ radius: Int, _ graphicsApi: GraphicsAPI) {
        self.init(graphicsApi)
        self.x = x
        self.y = y
        self.radius = radius
    }
    
    override func draw() -> Void {
        self.graphicsApi.drawCircle(self.x, self.y, self.radius)
    }
}

public class Rectangle : Shape {
    var x: Int = 0
    var y: Int = 0
    var width: Int = 0
    var height: Int = 0
    
    convenience init(_ x: Int, _ y: Int, _ width: Int, _ height: Int, _ graphicsApi: GraphicsAPI) {
        self.init(graphicsApi)
        self.x = x
        self.y = y
        self.width = width
        self.height = height
    }
    
    override func draw() -> Void {
        self.graphicsApi.drawRectangle(x, y, width, height)
    }
}

protocol GraphicsAPI {
    func drawRectangle(_ x: Int, _ y: Int, _ width: Int, _ height: Int)
    func drawCircle(_ x: Int, _ y: Int, _ radius: Int)
}

class DirectXAPI : GraphicsAPI {
    
    func drawRectangle(_ x: Int, _ y: Int, _ width: Int, _ height: Int) {
        print("Rectangle drawn by DirectXAPI API");
    }
    
    func drawCircle(_ x: Int, _ y: Int, _ radius: Int) {
        print("Circle drawn by DirectXAPI API");
    }
}

public class OpenGLAPI : GraphicsAPI {
    
    func drawRectangle(_ x: Int, _ y: Int, _ width: Int, _ height: Int) {
        print("Rectangle drawn by OpenGL API")
    }
    
    func drawCircle(_ x: Int, _ y: Int, _ radius: Int) {
        print("Circle drawn by OpenGL API")
    }
}

var openGLApi: OpenGLAPI = OpenGLAPI()
var directXApi: DirectXAPI = DirectXAPI()

var circle: Circle = Circle(10, 10, 10, openGLApi)
circle.draw()

circle = Circle(10, 5, 4, directXApi)
circle.draw()

var rect: Rectangle = Rectangle(10, 10, 10, 10, openGLApi)
rect.draw()

rect = Rectangle(10, 10, 10, 10, directXApi)
rect.draw()